<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `students`";
// var_dump($query);
include 'header.php'; 
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid margin-top">
            <a href="create-student.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Student</a>  
            <div class="table-responsive">     
            <table class="table table-hover margin-top">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>SEIP ID</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Address</th>
                  <th>Gender</th>
                  <th>Picture</th>
                  <th>Created At</th>
                  <th>Modified At</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                   <?php 
                  foreach($db->query($query) as $students) { ?>
                  <tr>
                    <td><?php echo $students['id'];?></td>
                    <td><?php echo $students['user_name'];?></td>
                    <td><?php echo $students['user_seip'];?></td>
                    <td><?php echo $students['user_email'];?></td>
                    <td><?php echo $students['user_number'];?></td>
                    <td><?php echo $students['user_address'];?></td>
                    <td><?php echo $students['user_gender'];?></td>
                    <td><img width="80px" src="images/<?php echo $students["user_pic"]; ?>"/> </td>
                    <td><?php echo date("d/m/Y", strtotime($students['created_at']));?></td>
                    <td><?php echo date("d/m/Y", strtotime($students['modified_at']));?></td>
                    <td>
                      <a href="view-single-student.php?id=<?php echo $students['id'];?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="edit-student.php?id=<?php echo $students['id'];?>" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="delete-student.php?id=<?php echo $students['id'];?>" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>
                <?php }
                ?>
              </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>