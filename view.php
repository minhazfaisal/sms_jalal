<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `course` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
?>



<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">     
          <a href="create.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New course</a>
          <a href="viewlist.php" class="btn btn-info"><i class="fa fa-eye"></i> view all course</a>      
          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>Subject Code</th>
                <th>Subject Title</th>
                <th>Department</th>
                <th>Shift</th>
              </tr>
            </thead>
            <tbody>
                 <?php 
                foreach($db->query($query) as $course) { ?>
                <tr>
                  <td><?php echo $course['id'];?></td>
                  <td><?php echo $course['subject_code'];?></td>
                  <td><?php echo $course['subject_title'];?></td>
                  <td><?php echo $course['department'];?></td>
                  <td><?php echo $course['shift'];?></td>
                </tr>
              <?php }
              ?>
            </tbody>
          </table>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
