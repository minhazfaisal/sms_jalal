<?php include 'header.php'; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'navigation.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid" style="margin-top: 20px;">
            <a href="view-student-list.php" class="btn btn-info margin-bottom"><i class="fa fa-eye"></i> View all Student</a>
              <div class="row">
                  <div class="col-md-12">
                      <fieldset>
                        <legend>Student Information:</legend>
                        <form action="store-student.php" method="post" enctype="multipart/form-data" class="form-horizontal" >
                            <div class="form-group">
                                <label class="control-label col-sm-3" >Name:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_name" value="" placeholder="Name" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" >SEIP ID:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_seip" value="" placeholder="SEIP ID" type="text" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Email:</label>
                                <div class="col-sm-9">
                                    <input class="form-control"  name="user_email" value="" placeholder="Enter Email" type="email" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Mobile:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_number" value="" placeholder="Enter Mobile" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Address:</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="user_address" value="" placeholder="Enter Address" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Gender:</label>
                                <div class="col-sm-9">
                                    <select name="user_gender" class="form-control" required="">
                                        <option value="Male">male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-sm-3">Upload File:</label>
                                <div class="col-sm-9">
                                    <input type="file" name="user_pic" class="form-control"></input>
                                </div>
                            </div>

                            <div class="form-group" style="margin-top:20px">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" name="submit" value="submit" class="btn btn-success">Submit</button>
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                </div>
                            </div>
                        </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
