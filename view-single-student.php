<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `students` WHERE id =".$_GET['id'];
// var_dump($query);
foreach($db->query($query) as $row) {
  $students = $row;
}
include 'header.php'; 
?>



<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid" style="margin-top: 20px;">
        <a href="create-student.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New Student</a>
        <a href="view-student-list.php" class="btn btn-info"><i class="fa fa-eye"></i> View All Student</a>      
          

            <div class="row" style="margin-top: 40px;">
              <div class="col-md-4">
                <img  src="images/<?php echo $students["user_pic"]; ?>" alt="" class="img-thumbnail profile-image">
              </div>
              <div class="col-md-8">
                
                <div class="pro-desc">
                  <h3><strong><?php echo $students['user_name'];?></strong></h3>

                  <table class="table table-striped">
                    <tbody>
                     <tr>
                      <td><strong>SEIP ID</strong></td>
                      <td><?php echo $students['user_seip'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Email</strong></td>
                      <td><a href="#"><?php echo $students['user_email'];?></a></td>
                    </tr>
                    <tr>
                      <td><strong>Phone</strong></td>
                      <td><?php echo $students['user_number'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Address</strong></td>
                      <td><?php echo $students['user_address'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Gender</strong></td>
                      <td><?php echo $students['user_gender'];?></td>
                    </tr>
                    <tr>
                      <td><strong>Created Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($students['created_at']));?></td>
                    </tr>
                    <tr>
                      <td><strong>Modification Date</strong></td>
                      <td><?php echo date("d/m/Y", strtotime($students['modified_at']));?></td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
          </div>

        </div>
    </div>
</div>

<?php include 'footer.php'; ?>
