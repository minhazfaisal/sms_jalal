<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `course`";
// var_dump($query);
include 'header.php'; 
?>

<div id="wrapper">
    <!-- Navigation -->
    <?php include 'navigation.php'; ?>

    <div id="page-wrapper">
        <div class="container-fluid margin-top">
            <a href="create.php" class="btn btn-success"><i class="fa fa-plus"></i> Add New course</a>       
            <table class="table table-hover margin-top">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Subject Code</th>
                  <th>Subject Title</th>
                  <th>Department</th>
                  <th>Shift</th>
                </tr>
              </thead>
              <tbody>
                   <?php 
                  foreach($db->query($query) as $course) { ?>
                  <tr>
                    <td><?php echo $course['id'];?></td>
                    <td><?php echo $course['subject_code'];?></td>
                    <td><?php echo $course['subject_title'];?></td>
                    <td><?php echo $course['department'];?></td>
                    <td><?php echo $course['shift'];?></td>
                    <td>
                      <a href="view.php?id=<?php echo $course['id'];?>" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i> Show</a>
                      <a href="edit.php?id=<?php echo $course['id'];?>" class="btn btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="delete.php?id=<?php echo $course['id'];?>" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Delete</a>
                    </td>
                  </tr>
                <?php }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>