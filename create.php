<?php include 'header.php'; ?>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'navigation.php'; ?>

        <div id="page-wrapper">
            <div class="container-fluid" style="margin-top: 20px;">
            <a href="viewlist.php" class="btn btn-info margin-bottom"><i class="fa fa-eye"></i> View all Course</a
              <div class="row">
                  <div>
                      <fieldset>
                          <legend>course Information:</legend>
                         <form action="store.php" method="POST" enctype="multipart/form-data" class="form-horizontal">
                             <div class="form-group">
                                 <label class="control-label col-sm-3">Subject Code:</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" name="sub_code"  placeholder="Subject Code">    
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label col-sm-3">Subject Title:</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" name="sub_title"  placeholder="Subject Title">  
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label col-sm-3">Department:</label>
                                 <div class="col-sm-9">
                                    <input type="text" class="form-control" name="department"  placeholder="Department">    
                                 </div>
                             </div>
                             <div class="form-group">
                                <label class="col-sm-3 control-label">Shift:</label>
                                <div class="col-sm-9">
                                    <select name="shift" class="form-control" required="">
                                        <option value="Day">Day</option>
                                        <option value="Evening">Evening</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-default btn-primary">Submit</button>
                             </div>
                         </form>
                     </fieldset>
                  </div>
              </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
